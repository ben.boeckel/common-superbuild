superbuild_add_project_python_wheel(pythonmpi4py
  DEPENDS mpi
  LICENSE_FILES_WHEEL
    mpi4py-${mpi4py_ver}.dist-info/LICENSE.rst)
